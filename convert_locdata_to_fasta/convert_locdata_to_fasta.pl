#!/usr/bin/perl
#usage: perl locdata_to_fasta.pl locdata.txt output.fasta
#This script converts the locdata.txt output of Gibpps to a fasta file with all loci
#The resulting fasta file can then be used for other purposes (e.g. mapping against a reference)

#set environment
$|=1;
use strict;
use warnings;

################
#set parameters#
################
my ($inputfile,$outputfile,$lijn,@line);
$inputfile= $ARGV[0];
open (IP,$inputfile) || die "cannot open \"$inputfile\":$!";
$outputfile= $ARGV[1];
open (OP,">$outputfile");
die "usage perl locdata_to_fasta.pl locdata.txt output.fasta" if (scalar @ARGV != 2);

#########
#script #
#########
while ($lijn=<IP>)	
	{	chomp $lijn;
		next if $. < 2;	#skip first line with header
		@line = split (/\t/,$lijn);
		print OP ">",$line[0],"\n";
		print OP $line[2],"\n";
		}
