#!/bin/bash
# (c) Annelies Haegeman 2018
# annelies.haegeman@ilvo.vlaanderen.be
# This bash script is used to calculate a phylogenetic tree from the output of Gibpss.
# It consists of the following steps:
# Step 1: the genotypes file is converted to fasta files (1 file per locus), each allele is mentioned separately in the fasta file.
# Step 2: new alignments (in phylip format) are made where the invariate SNPs are removed for each locus
# Step 3: A phylogenetic tree is made per locus, including 100 bootstraps
# Step 4: A tree is inferred using ASTRAL with local posterior probabilities
#####################################################

###################
#PARAMETER SETTINGS
###################
#Specify here the folder where all scripts needed for this analysis are:
SCRIPTS_FOLDER=/home/genomics/kvanpoucke/scripts/phylogeny
#Specify here the folder where your genotypes file is:
INPUT_FOLDER=/home/genomics/kvanpoucke/Gibpss_Phytophthora_5/phylogeny/all_clades
#Specify here the name of your genotypes file:
GENOTYPES_FILE=genotypes_sharedloci_30.txt
#Specify the number of threads you want to use while running RAXML
THREADS=12

date
echo
echo $INPUT_FOLDER
echo $GENOTYPES_FILE
echo

######################################################
#STEP1: convert genotypes file to separate fasta files
######################################################
echo "Step 1: converting genotypes file to fasta files per locus..."

#change directory to folder where the data is
cd $INPUT_FOLDER
mkdir trees #make a directory "trees". A directory called "fasta_files" is also created by the perl script to convert the genotypes file to fasta files.
#Run perl script to create fasta files for each locus. These are stored in a newly created folder called "fasta_files".
#The script also creates a mappingfile.txt, this file maps the names of the alleles to the isolate names and will be used for NJstm.
perl $SCRIPTS_FOLDER/convert_genotypes_to_alignments_per_locus.pl $GENOTYPES_FILE

echo
echo "Done with step 1: converting genotypes file to fasta files per locus."
echo
##############################
#STEP2: remove invariante SNPs
##############################
echo "Step 2: removing invariant SNPs in all fasta files..."

#STEP 2.1: MAKE PHYLIP FILES
# Define the variable FILES that contains all the fasta files. 
FILES=($INPUT_FOLDER/fasta_files/*.fasta)
#Loop over all files and make phylip files to use as input for the script "extractVariantSites.py"
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f .fasta`
echo
echo "Making phylip file from fasta file $SAMPLE ..."
#MAKE PHYLIP FILE
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta -getPhylip
echo "Done making phylip file from fasta file $SAMPLE."
done

#STEP 2.2: USE ascbias.py SCRIPT TO REMOVE INVARIANT SITES FROM PHYLIP FILES, see https://github.com/btmartin721/raxml_ascbias
#now loop over all phylip files
FILES=($INPUT_FOLDER/fasta_files/*.fasta.phy)
for f in "${FILES[@]}" 
do 
#Define the variable SAMPLE who contains the basename where the extension is removed
SAMPLE=`basename $f .fasta.phy`
echo
echo "Remove invariant SNPs for $SAMPLE ..."
python3 $SCRIPTS_FOLDER/ascbias.py -p $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta.phy -o $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs.phy

echo "Done removing invariant SNPs for $SAMPLE."
done
echo
echo "Done with step 2: removing invariant SNPs in all fasta files."
echo
##############################
#STEP3: Make phylogenetic trees per locus
##############################
echo "Step 3: Making trees per locus..."
FILES=($INPUT_FOLDER/fasta_files/*_SNPs.phy)
for f in "${FILES[@]}" 
do 
SAMPLE=`basename $f _SNPs.phy`
echo "Running RAXML on $SAMPLE ..."
raxml -m ASC_GTRCAT -V --asc-corr=lewis -s $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs.phy -p 12345 -n $SAMPLE -T $THREADS -w $INPUT_FOLDER/trees >> $INPUT_FOLDER/trees/raxml_log.txt
echo "Done running RAXML on $SAMPLE."
echo
done
echo "Done with step 3: making trees per locus."
echo

#########################################
#STEP4: Perform ASTRAL on all locus trees
#########################################
echo "Step 4: Making consensus tree using ASTRAL III..."
#make directory to store trees
mkdir $INPUT_FOLDER/trees/Astral
#make file containing all best ML trees
cat $INPUT_FOLDER/trees/RAxML_bestTree.* > $INPUT_FOLDER/trees/Astral/all_locus_trees.txt
#run Astral
java -Xmx5000M -jar /usr/local/bioinf/Astral/astral.5.6.2.jar -i $INPUT_FOLDER/trees/Astral/all_locus_trees.txt -o $INPUT_FOLDER/trees/Astral/astral_output.txt 2>$INPUT_FOLDER/trees/Astral/astral.log
#java -Xmx5000M -jar /usr/local/bioinf/Astral/astral.5.6.2.jar -i $INPUT_FOLDER/trees/Astral/all_locus_trees.txt -q $INPUT_FOLDER/trees/Astral/astral_output.txt -o $INPUT_FOLDER/trees/Astral/astral_output_withlocalbranchsupport.txt 2>$INPUT_FOLDER/trees/Astral/astral.log
echo
echo "Done with step 4: Making consensus tree using ASTRAL III."
echo

################################################################################################################################
#EXTRA: Count the number of files (=number of loci) and the number of invariate sites by looking at the first line in the phylip files
################################################################################################################################

FILES=($INPUT_FOLDER/fasta_files/*_SNPs.phy)
for f in "${FILES[@]}" 
do 
SAMPLE=`basename $f _SNPs.phy`
LOCI=$((LOCI+1))
SITES=`head -n 1 $INPUT_FOLDER/fasta_files/"$SAMPLE"_SNPs.phy | cut -d ' ' -f2`
COUNTS=$((COUNTS+SITES))
#rm $INPUT_FOLDER/fasta_files/"$SAMPLE".fasta.phy		#remove intermediate phylip file
done
echo
echo "Done with analysis. $LOCI loci containing $COUNTS variable sites were included."

echo
date
