#!/usr/bin/perl

$|=1;
use strict;
#use warnings;
use List::Util qw( reduce );

#############
#input files#
#############
my $datafile= $ARGV[0];
my $outputfile=$ARGV[1];
die "usage perl convert_SNPalignment_to_01.pl alignment.phy output.txt" if (scalar @ARGV != 2);

#####################
#initiate variabeles#
#####################
my ($lijn,@line,$numberofsamples,$length,@metrics,$sample,$seq,%data_per_position,%data_per_sample,%filtered_data_per_sample,@samples,%strings,@keep,$check,$element,@discard,$uniq,$position,%headcharacter,%count,$max_val_key);
my $i=0;
my $j=0;
my $k=0;
my $l=0;
my $m=0;
my $aberrant=0;
open(OP1,">".$datafile."_filtered.fasta");
open(OP2,">$outputfile");

########
#SCRIPT#
########

#read phylip file and store all bases in a hash with as keys the sample ID and the position in the alignment
open (IP,$datafile) || die "cannot open \"$datafile\":$!";
while ($lijn=<IP>)		
	{	chomp $lijn;
		if ($. == 1){		#read number of samples and number of SNPs from first line
			@metrics = split (/\s/,$lijn);
			$numberofsamples=$metrics[0];
			$length=$metrics[1];
		}
		else{	#from second line on, store data_per_position in hash of hashes
			#prepare data_per_position
			@line = split (/\t/,$lijn);
			$sample=$line[0];	#sample
			$seq=$line[1];		#sequence
			push (@samples,$sample);
			
			#store sequence data_per_position per sample in a hash with as key the sample ID and as value the sequence
			$data_per_sample{$sample}=$seq;
			
			#loop over all bases of the sequence and store the corresponding base in a hash with keys the sample name and the base number
			while ($i<$length){
				$data_per_position{$sample}{$i+1}=substr($seq, $i, 1); 
				$i++;
				}
			$i=0;	
		}
	}	

#now loop over each position in the alignment and store all characters of a certain position in a string
while ($l < $length){
	foreach $sample (@samples){
		$strings{$l+1}=$strings{$l+1}.$data_per_position{$sample}{$l+1}	#hash with as key the position in the alignment, and as value a string of all observed bases for that position over all samples
	}
	$l++;
}

#check
#while ($k < $length){
#	print "$strings{$k+1}\n";
#	$k++;
#}

#loop over all positions, check the string and determine whether the position should be kept or discarded + check the most occurring character in the string
while ($k < $length){
	$aberrant=$strings{$k+1}=~ /\-|N|R|Y|S|W|K|M|B|D|H|V/i; #check for aberrant characters, if present, $aberrant will have value 1.
	$uniq=number_of_uniques($strings{$k+1});	#check the number of different/unique bases. Should be exactly 2 if we only keep biallelic SNPs.
	if ($aberrant > 0 or $uniq != 2 ){			#to discard = $aberrant>0, $uniq != 2
		push(@discard,$k+1);	#add position in alignment to array of positions to discard
	}
	else{
		push(@keep,$k+1);		#add position in alignment to array of positions to keep
	}
	#print "$strings{$k+1}\t$aberrant\t$uniq\n";	#check
	
	#check which character occurs most for each position and store info in hash %headcharacter with as key the position in the alignment and as value the most occurring character
	#to do this, first store counts of each character in a hash
	foreach my $str (split //, $strings{$k+1}) {
		$count{$str}++;
		}
	#now find the key of the hash with the maximum value
	$headcharacter{$k+1} = reduce { $count{$a} > $count{$b} ? $a : $b } keys %count;	#find key for which the value is the maximum of the complete hash
	#print "$k+1\t$strings{$k+1}\t$headcharacter{$k+1}\n";	
	#make hash with counts empty
	%count=();
	
	#reset variables
	$k++;
	$aberrant=0;	
}


#check
#foreach $element (@keep){
#	print "$element\t";
#	print "$headcharacter{$element}\n";
#}

#loop over all samples and 1) only print the sequence of the positions to keep to a new alignment file and 2) make a new file containing 1/0 values
while ($m < $numberofsamples){
	$sample=$samples[$m];
	print OP2 "$sample\t";
	foreach $position (@keep){
		#check if base on that position is the same or different as the most frequent character at that position and assign 1 or 0 to it
		if (substr($data_per_sample{$sample},$position-1,1) eq $headcharacter{$position}) {
			#print OP2 $position.substr($data_per_sample{$sample},$position-1,1).$headcharacter{$position}." 1\t";
			print OP2 "1\t";
		}
		else {
			#print OP2 $position.substr($data_per_sample{$sample},$position-1,1).$headcharacter{$position}." 0\t";
			print OP2 "0\t";
		}
		$filtered_data_per_sample{$sample}=$filtered_data_per_sample{$sample}.substr($data_per_sample{$sample},$position-1,1);	#store DNA alignment to keep
	}
	$m++;
	print OP2 "\n";
	print OP1 ">$sample\n";
	print OP1 "$filtered_data_per_sample{$sample}\n";	
}	

#function to calculate the number of unique characters in a string
sub number_of_uniques {
   my %s;
   return scalar (grep !$s{$_}++, split '', $_[0]);
}

#check
#foreach $sample (@samples){
#	print "$sample\t";
#	while ($j<$length){
#		print "$data_per_position{$sample}{$j+1}";
#		$j++;			
#	}
#	$j=0;
#	print "\n";
#}
