# GBS_Phytophthora

This repository contains scripts for the processing of Genotyping-by-Sequencing data from Phytophthora isolates. It consists of scripts for preprocessing of the data (from raw data to cleaned reads), and also for downstream analyses after analysis with [Gibpss](https://github.com/ahapke/gibpss) (reference-free GBS analysis software).
Important remark: the scripts were made for our local server and may need some updating before they will run on your system (for example absolute paths to external software should be updated).

All scripts are distributed under the [MIT license](https://opensource.org/licenses/MIT).

## Installation requirements

The following software was used to develop and run the scripts in this repository (newer versions of the software might work as well, but this was not tested).

* Operating system: Ubuntu 15.04
* [perl](https://www.perl.org/) 5.20.2
* [Bioperl](https://bioperl.org/) 1.006924
* [GNU Parallel](https://www.gnu.org/software/parallel/) (should be in $PATH, it is called in the scripts as `parallel`)
* [cutadapt](https://cutadapt.readthedocs.io/en/stable/guide.html) 1.16 (should be in $PATH, it is called in the scripts as `cutadapt`)
* [FASTX Toolkit](http://hannonlab.cshl.edu/fastx_toolkit/) 0.0.14 (all programs should be in $PATH, it is called in the scripts as `fastx_trimmer` and `fastq_masker`)
* [python](https://www.python.org/) 2.7 (should be in $PATH, it is called in the scripts as `python`)
* Python packages for python 2.7: [Biopython](https://biopython.org/) 1.65
* [python](https://www.python.org/) 3.4.3 (should be in $PATH, it is called in the scripts as `python3`)
* Python packages for python 3.4.3: [os](https://docs.python.org/3/library/os.html), [sys](https://docs.python.org/2/library/sys.html), [argparse](https://docs.python.org/3/library/argparse.html), [datetime](https://docs.python.org/2/library/datetime.html), [multiprocessing](https://docs.python.org/2/library/multiprocessing.html), [pandas](https://pandas.pydata.org/), [shelve](https://docs.python.org/3/library/shelve.html)
* [PEAR](https://cme.h-its.org/exelixis/web/software/pear/) 0.9.8 (should be in $PATH, it is called in the scripts as `pear`)
* [GBSX](https://github.com/GenomicsCoreLeuven/GBSX) 1.5.0 (note that in the Demultiplexing script, you will need to adjust the path to GBSX to your local path)
* [prinseq-lite](https://github.com/b-brankovics/grabb/blob/master/docker/prinseq-lite.pl) 0.20.4 (should be in $PATH, it is called in the scripts as `prinseq-lite`)
* [obitools](https://pythonhosted.org/OBITools/welcome.html) 1.01 22 (should be in $PATH, it is called in the scripts as `obigrep`)
* [pairfq](https://github.com/sestaton/Pairfq) 0.14 (should be in $PATH, it is called in the scripts as `pairfq`)
* [jModeltest](https://github.com/ddarriba/jmodeltest2) 2.1.10 v20160303  (note that in the ASTRAL coalescent tree script, you will need to adjust the path to jModeltest to your local path)
* [ascbias.py](https://github.com/btmartin721/raxml_ascbias) script from https://github.com/btmartin721/raxml_ascbias (place it in your SCRIPTS folder as explained later)
* [RAxML](https://cme.h-its.org/exelixis/web/software/raxml/index.html) 8.2.10  (should be in $PATH, it is called in the scripts as `raxml`)
* [Astral](https://github.com/smirarab/ASTRAL) 5.6.2 (note that in the ASTRAL coalescent tree script, you will need to adjust the path to Astral to your local path)
* [RStudio](https://www.rstudio.com/) with [R](https://www.r-project.org/) 3.4.0 including the following packages: data.table, ggplot2, vegan, fastcluster, pvclust, factoextra, cluster, ggdendro, phangorn, latticeExtra, ggrepel, stringr


## Preprocessing

###	Introduction
This pipeline describes how to analyze GBS data of Phytophthora strains (Resipath project). The double digest GBS libraries were prepared using an adapted protocol of (Elshire et al., 2011) and (Poland et al., 2012), with restriction enzymes PstI and HpaII (= MspI isoschizomer). Resulting libraries were paired end sequenced (2x150 bp). This data analysis pipeline is specifically tailored for this protocol, i.e. double digest GBS and paired end sequencing. It basically consists of two major steps:
* 1) Preprocessing of the data
* 2) Reference-free locus identification and comparison

The preprocessing of the data is done with custom scripts which were based on the single digest GBS data analysis workflow of Christophe Verwimp (GBS of ryegrass and comb jelly). The reference-free locus identification is mainly done using the software GIbPSs (Hapke and Thiele, 2016).
The preprocessing of the GBS data consists of 4 steps:
* 1) Demultiplexing
* 2) Trimming of adapters and restriction site remnants
* 3) Merging of F and R reads
* 4) Quality filtering
The preprocessing scripts are structured as follows. There are main scripts (`GBS_Demultiplexing_v2.sh`, `GBS_Trimming_v7_PE.sh`, `GBS_Merging_v1.sh`, `GBS_QualityFiltering_v7.sh`, `FastQ_CountReads.py`, `FastQ_TrimInternalRestrictionSites.py`) which do the actual processing, these scripts should be placed together in a single folder. Next to the main scripts, there are so-called Wrapper scripts: these scripts refer to the main scripts and contain all parameters necessary to run the analysis (`WrapperScriptDemultiplexing.sh`, `WrapperScriptTrimming.sh`, `WrapperScriptMerging.sh`, `WrapperScriptQualityFiltering.sh`). You have to open the Wrapper scripts in your favorite text editor (for example [Notepad++](https://notepad-plus-plus.org/)) and adjust the parameters as explained below.

###	Create directory tree and prepare input files
To nicely structure your data, it is highly recommended to create a number of directories for the different steps in the preprocessing. You can for example make a directory “Preprocessing” which contains the subdirectories “Scripts”, “Demultiplexing”, “Trimming”, “Merging”, “QualityFiltering”. Each of these folders (except “Scripts”) can have a subfolder “Log” and a subfolder “Output”.
To easily create all these folders in a single step, execute the following command:
```
mkdir –p Preprocessing/{Scripts,Demultiplexing/{Log,Output},QualityFiltering/{Log,Output},Trimming/{Log,Output},Merging/{Log,Output}}
```
Next, copy all scripts to the “Scripts” folder.
Finally, we need to prepare some input files which are needed for the different scripts: a `TableOfBarcodes.txt` file and a `ListOfSamples.txt` file.
The `TableOfBarcodes.txt` file is a tab separated file containing the following information:
```
column 1	sample name
column 2	barcode sequence
column 3	restriction enzyme (compatible to barcode adapter)
column 4	optionally, second restriction enzyme
column 5	optionally, second barcode sequence
```
The `ListOfSamples.txt` file is a file which contains 1 column with on each line the name of the sample which needs to be included in the analysis.

If you created these files in Windows, do not forget to convert them to Unix format. This can be done from the command line using [dos2unix](http://dos2unix.sourceforge.net/) as follows:
```
dos2unix TableOfBarcodes.txt
dos2unix ListOfSamples.txt
```

###	Demultiplexing
The first step of the preprocessing consists of demultiplexing of the raw data, i.e. splitting the reads according to the sample from which they were derived. You can allow a maximum number of mismatches to the barcode. The barcode is subsequently clipped off and the reads are put in separate files according to the sample.
Open the `WrapperScriptDemultiplexing.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the demultiplexed files
LogDirectory= location where the program should put a log file
RawDataForward= location of the fastq file with forward reads
RawDataReverse= location of the fastq file with reverse reads
TableOfBarcodes= TableOfBarcodes.txt file
NumberOfMismatchesAllowed= maximum number of allowed mismatches in the barcode
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Log"
RawDataForward="/home/genomics/ALL_RAW_SEQ_DATA/GBS/Resipath/forward_reads.fq"
RawDataReverse="/home/genomics/ALL_RAW_SEQ_DATA/GBS/Resipath/reverse_reads.fq"
TableOfBarcodes="/home/genomics/kris/1_Preprocessing/TableOfBarcodes.txt"
NumberOfMismatchesAllowed=1
```
Next, run the Demultiplexing script as follows:
```
bash WrapperScriptDemultiplexing.sh
```
The input `TableOfBarcodes.txt` is ordered on descending barcode length to avoid wrong assignment when using high numbers of allowed mismatches. The raw fastq files are read and demultiplexed using the software [GBSX](https://github.com/GenomicsCoreLeuven/GBSX) (Herten et al., 2015). An F for 'forward' and R for 'reverse' is added to the names of sample specific FastQ files. Undetermined reads are stored in a separate FastQ file. A new file is created (e.g. RawData_output.tbl), containing the sample names and the numbers of reads per sample after demultiplexing. Samples with low numbers of reads should be excluded for further processing.

IMPORTANT NOTE: in the `GBS_Demultiplexing_v2.sh` script (line 66), you should change the path to GBSX to the path on your system where GBSX is found.

### Trimming
After the demultiplexing, the reads are trimmed. This is done in two steps. First the adapter at the 3’ end is cut off including the remainder of the restriction site. Next, the remainder of the restriction site at the 5’ end is clipped off. The script uses both the cutadapt (Martin, 2011) and [FastX](http://hannonlab.cshl.edu/fastx_toolkit/index.html) toolkit software. It also works in parallel to go faster using [GNU parallel](https://www.gnu.org/software/parallel/).
Open the `WrapperScriptTrimming.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
LogDirectory= location where the program should put a log file
OutputDirectory= location where the program should put the trimmed files
InputDirectory= location of the input files (output of demultiplexing)
TableOfBarcodes= TableOfBarcodes.txt file
Sequencing= “SE” for single end sequencing and “PE” for paired end sequencing
ReadLength= exact length of the input reads
MinimumLength= minimum length of reads to keep, put this at min. 10 to avoid broken pairs later on
MaxNumberOfCores= number of processors the program can use
DeleteIntermediateFiles= whether you want to delete intermediate files ("FALSE" or “TRUE”)
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing_bis/Scripts"
LogDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Log"
OutputDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Output"
InputDirectory="/home/genomics/kris/1_Preprocessing/Demultiplexing/Output"
TableOfBarcodes="/home/genomics/kris/1_Preprocessing_bis/TableOfBarcodes.txt"
Sequencing="PE"
ReadLength=151
MinimumLength=10
MaxNumberOfCores=16
DeleteIntermediateFiles="FALSE"
```
Next, run the Trimming script as follows:
```
bash WrapperScriptTrimming.sh
```

###	Merging
If the size of the fragments was relatively low, there is a great chance that forward and reverse reads can overlap. In order to avoid having two separate loci (one derived from the F read and one from the R read) that are in reality only one locus, it is best to check if F and R reads are overlapping and can be merged. In this step we check whether that is the case or not using the software [PEAR](https://cme.h-its.org/exelixis/web/software/pear/) (Zhang et al., 2014). The software takes into account the quality of the bases, in case there is a mismatch in the overlap, it will select the base with the highest quality and it will adjust the quality values accordingly. If reads can be merged, they are added to a new file with merged reads. If they cannot be merged, they remain in two separate files containing F and R reads.
Open the `WrapperScriptMerging.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the merged files
LogDirectory= location where the program should put a log file
InputDirectory= location of the input files (output of trimming)
ListOfSamples= ListOfSamples.txt file
MinOverlap= minimum overlap needed between F and R read in order to start merging
MaxNumberOfCores= number of processors the program can use
MaxMemory= maximum amount of memory the program can use (e.g. “30G” for 30 GB RAM)
MinLength= minimum length of merged reads (smaller merged fragments are discarded)
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing_bis/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing_bis/Merging/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing_bis/Merging/Log"
InputDirectory="/home/genomics/kris/1_Preprocessing_bis/Trimming/Output"
ListOfSamples="/home/genomics/kris/1_Preprocessing_bis/ListOfSamples.txt"
MinOverlap=10
MaxNumberOfCores=16
MaxMemory="30G"
MinLength=25
```
Next, run the Merging script as follows:
```
bash WrapperScriptMerging.sh
```

###	Quality Filtering
The final step in the preprocessing is the quality filtering. This script does the following things:
* Bases with quality value lower than x are replaced by N. If you do not want this substitution, just use a very low quality threshold (e.g. Q<1).
* An extra trimming step is done of x bases at the 3’ end of the remaining paired reads (not the merged reads). This is because the last part of the sequence is typically of low quality.
* Sequences with a mean quality less than x are removed.
* Sequences shorter than x bp are removed.
* Sequences with more than x % N’s are removed

Of the quality filtered files, a copy is made where the reads that still contain internal restriction sites are removed.
To do all these steps, the script uses the software [FastX toolkit](http://hannonlab.cshl.edu/fastx_toolkit/index.html), [prinseq-lite](https://github.com/b-brankovics/grabb/blob/master/docker/prinseq-lite.pl) (Schmieder and Edwards, 2011), [obitools](https://pythonhosted.org/OBITools/welcome.html) (Boyer et al., 2015) and [pairfq](https://github.com/sestaton/Pairfq). The script keeps the merged files, the remaining pairs and the resulting singletons after filtering in separate files.
Open the `WrapperScriptQualityFiltering.sh` using a text editor and change the following parameters:
```
ScriptsDirectory= location of the scripts
OutputDirectory= location where the program should put the quality filtered files
LogDirectory= location where the program should put a log file
InputDirectory= location of the input files (output of merging)
ListOfSamples= ListOfSamples.txt file
MinMeanQuality= Minimum mean quality a read should have
MinLength= Minimum length a read should have
MinBaseQuality= A base with a lower quality of this is converted into an N
MaxNumberOfNs= Maximum percentage of N’s allowed. If more than x % of the bases are N’s, the read is removed.
MaxNumberOfCores= number of processors the program can use
TrimmingLengthOfNonMergedReads= number of bases that can be trimmed of the 3’ part of non merged reads
```
For example:
```
ScriptsDirectory="/home/genomics/kris/1_Preprocessing/Scripts"
OutputDirectory="/home/genomics/kris/1_Preprocessing/QualityFiltering/Output"
LogDirectory="/home/genomics/kris/1_Preprocessing/QualityFiltering/Log"
InputDirectory="/home/genomics/kris/1_Preprocessing/Merging/Output"
ListOfSamples="/home/genomics/kris/1_Preprocessing/ListOfSamples.txt"
MinMeanQuality=25
MinLength=30
MinBaseQuality=1
MaxNumberOfNs=10
MaxNumberOfCores=16
TrimmingLengthOfNonMergedReads=30
```
Next, run the QualityFiltering script as follows:
```
bash WrapperScriptQualityFiltering.sh
```

The output folder will contain the following files:
* A quality filtered file with merged reads
* Two quality filtered files with read pairs (file with F read and file with R reads)
* A quality filtered file with singleton reads
* A copy of the four previous files where the reads with internal restriction sites were removed.

Also an output table is generated with the remaining number of reads in each file for each sample.


## Parent hybrid comparison

The `parent_hybrid_comparison.py` script compares all isolates to each other. It uses the `genotypes.txt` file as generated  by the Gibpss software. Two things are calculated:
1) the number of loci in common : this gives the absolute number of loci that occur in both isolates.
2) the allele similarity (in %) : this gives the percentage of loci that two isolates have in common, that share at least 1 allele.
These two values are given for all one-to-one isolate comparisons in the form of similarity tables: `commonloci.txt` and `snpsim.txt`.

Example: isolate1 and isolate2 have 20.000 loci in common and have an allele similarity of 5.0%. This means that 1000 loci (5% of 20.000) have an allele which occurs in both isolates, without any SNPs. If there is at least 1 nucleotide difference between the alleles, the corresponding locus is not counted in the allele similarity.

```
python3 parent_hybrid_comparison.py genotypes.txt
```

## Stacked barplot heterozygosity

The `stacked_barplot_heterozygosity.R` script is an R script that reads the `genotypes.txt` output file from Gibpss as well as a `metadata.txt` file with extra metadata to be mentioned in the graph. It then makes a stacked bar chart which represents the number of loci per isolate, colored by the number of alleles per locus.

## Loci in which isolates

The `loci_in_which_isolates.R` script is an R script that reads the `genotypes.txt` output file from Gibpss as well as a file containing locus numbers. It then checks in which isolates these locus numbers occur and in what numbers.

## Convert locdata to fasta

The `convert_locdata_to_fasta.pl` script is a perl script that reads the `locdata.txt` output file from Gibpss and converts it to a fasta file.

```
perl convert_locdata_to_fasta.pl locdata.txt locdata.fasta
```


## Filter loci for missing data

The `loci_shared_by_all.R` script is an R script that will remove loci with a certain amount of missing data in the following steps:
1)	Read the `genotypes.txt` file from your Gibpss analysis
2)	Read the `annotations.txt` file containing annotations (extra info) on your samples (tab delimited file)
3)	Make a subselection of the isolates present in your `annotations.txt` file (file with metadata)
4)	Make a subselection of loci that occur in all isolates of interest, in 70% of the isolates of interest, in 50% of the isolates of interest and of 30% of the isolates of interest.
5)	Print lists of these loci to the output
6)	Reconsider the original genotypes.txt file from your Gibpss analysis, again subselect the isolates of interest from your annotations.txt file and finally write different subsetted `genotypes.txt` files to the output containing only the data from the loci as described in 4).

Another script is available in this folder: `select_loci_and_inds_from_genotypes.R`. This script reads three files:
1) `genotypes.txt` file
2) file with a list of loci of interest (one locus number per line)
3) annotations file, tab delimited file with a list of individuals of interest, with extra info added.

The script will subselect the loci (of the loci file) and individuals (of the annotations file) from the genotypes file, and write a new (subsetted) genotypes file to output. It will also rename the individuals (column isolate in the annotations file) to a more informative name (column name in the annotations file).


## Hierarchical clustering

The `hierarchical_clustering.R`script is an R script that reads the `genotypes.txt` output file from Gibpss. It will then convert the file to a presence/absence object (1/0), and based on this data, a hierarchical clustering analysis (UPGMA) is done by using the Jaccard distance.

## Phylogeny

###	Introduction
Reference free analysis of GBS data using the software Gibpss results in several output files, among which the `genotypes.txt` file contains most data. In this file, for each locus/sample combination, the exact nucleotides for all alleles at the different SNP positions in the locus is listed. In case the locus is absent, “-999” is shown. The scripts described in this workflow all start from this file to make dendrograms/phylogenetic trees. The first part of this workflow (described in point 2) selects the isolates of interest from a genotypes file, and only output the loci that occur in at least xx % of the isolates of interest. This is to avoid that too many loci are included with missing data (absence) over the samples. In the next points, different methods to obtain trees are described:
* Coalescent trees with posterior probabilities (species trees made from trees per locus)
* Maximum likelihood tree from concatenated SNPs

###	Make new genotypes file containing isolates of interest and data from shared loci
First we need to select the isolates of interest from the “genotypes.txt” file (output from Gibpss). The genotypes.txt file contains all SNP information of all loci from all isolates. To select isolates of interest, you need to make a tab separated text file called `annotations.txt`, which should have a column called “isolate” and a column called “name” for all isolates you want to analyze. The “isolate” column should match the names of the isolates as they were used in the Gibpss analysis, the “name” column is the name you want the isolate to have in your subsequent analyses. This is an example of an `annotation.txt` file:
```
isolate	name
TJ169	CAC1_1a_TJ169
MR10	sp1_1a_MR10
MR16	sp2?_1a_MR16
06_018	HED_1a_06_018
```
Next, open Rstudio and open the script called `loci_shared_by_all.R`(in the folder "filter_loci_for_missing_data"). This script will do the following:
1)	Read the `genotypes.txt` file from your Gibpss analysis
2)	Read the `annotations.txt` file
3)	Make a subselection of the isolates present in your annotations.txt file
4)	Make a subselection of loci that occur in all isolates of interest, in 70% of the isolates of interest, in 50% of the isolates of interest and of 30% of the isolates of interest.
5)	Print lists of these loci to the output
6)	Reconsider the original `genotypes.txt` file from your Gibpss analysis, again subselect the isolates of interest from your `annotations.txt` file and finally write different subsetted `genotypes.txt` files to the output containing only the data from the loci as described in 4).

To run the script, make sure to adjust the working directory and the correct `genotypes.txt` file in the beginning of the script.

### Run shell script to make coalescent tree
#### How to run the script
The shell script `Make_coalescent_phylogenetic_trees_ASTRAL_postprob.sh` takes as input a `genotypes.txt` file as output by Gibpps. It is strongly advised to first subselect the isolates you want to include in the analysis, and only select the loci that occur in most of these isolates (for example the loci that occur in at least 70% of all isolates), as explained in the previous step. To limit computing time, it is advised not to analyse more than 1000 loci.
To prepare the analysis you should:
1)	Make a folder containing all scripts necessary for the shell script to function:
a.	`convert_genotypes_to_alignments_per_locus_all_alleles.pl`
b.	`ascbias.py` from https://github.com/btmartin721/raxml_ascbias
2)	Open the script `Make_coalescent_phylogenetic_trees_ASTRAL_postprob.sh` and adjust the following info:
a.	SCRIPTS_FOLDER : folder where you put all scripts mentioned in the previous point (!! Don’t add a slash to the end of the folder name)
b.	INPUT_FOLDER : folder where you put your input genotypes.txt file, this folder will also be used to put all results in.
c.	GENOTYPES_FILE : name of the genotypes file
d.	THREADS : number of threads you want to use in the RAxML analysis
e.  Adjust the path to the jModeltest jar file to the path on your computer (line 61)
f.  Adjust the path to the Astral jar file to the path on your computer (line 106)
To run the script:
```
bash Make_coalescent_phylogenetic_trees_ASTRAL_postprob.sh > log.txt 2>&1
```
All output from the script will be saved in a file called `log.txt`, which is useful in case of errors.

#### Detailed info on the different steps of the script
##### STEP 1: Convert genotypes file to fasta files per locus
In this step the genotypes file is converted to multiple fasta files per locus. This is done using the custom perl script `convert_genotypes_to_alignments_per_locus_all_alleles.pl`.
```
perl convert_genotypes_to_alignments_per_locus.pl input_genotypes.txt
```
As output, the script creates a folder called `fasta_files`. In this folder, there are fasta files for each locus with the alignment of the data that was found in the `genotypes.txt` file for all of the samples present in that file. When a locus is absent from a certain sample, there is nothing in the resulting fasta file. All separate alleles from 1 sample are combined to a single sequence in the fasta file, using the IUPAC degeneracy code when the bases are different in the alleles.
##### STEP 2: Remove invariant sites from the alignments
Since you work with a subset of samples, there are typically still invariant sites present in your subsetted genotypes file. In the phylogenetic method we want to apply, we need to correct for ascertainment bias (see step 3.2.3). To be able to do this, no invariant sites are allowed in your fasta alignment files. Therefore the invariant sites are removed using the script `ascbias.py` from  https://github.com/btmartin721/raxml_ascbias. The input file type for this script is `phylip` format. This means the fasta alignment files first need to be converted to phylip files. This can be done using the software `jModeltest` (Darriba et al., 2012) (use the path to jModelTest on your computer).
```
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d inputfile.fasta –getPhylip
```
After the conversion to phylip files, the invariant sites can be removed. We use the `ascbias.py` script from https://github.com/btmartin721/raxml_ascbias .
```
python3 ascbias.py -p input.fasta.phy -o outputfilenamebase
```
##### STEP 3: Make phylogenetic trees per locus
Once there are alignment files (in phylip or fasta format, in this case phylip format) availalble, you can construct phylogenetic trees using the software RAxML (Stamatakis, 2014). RAxML uses maximum likelihood, and is computationally optimized to work with large datasets.
```
raxml -m ASC_GTRCAT –V --asc-corr=lewis -s inputalignment.phy -p 12345 -n name -T number_of_threads -w /absolute/path/to/output/folder >> raxml_log.txt
```
Options:
`-m` Evolutionary model to use. GTRGAMMA is the preferred model to use in RAxML. When calculations are too slow, you can use GTRCAT (recommended if you have more than 50 taxa). The “ASC_” is added because we want to correct for ascertainment bias. This is necessary when you work with variable sites only (in our case, concatenated SNPs).
`-V` : disable rate heterogeneity (widely applied if you work with multilocus data)
`--asc-corr` : the ascertainment bias correction model to be used. Here the simplest method is used (lewis correction). Other correction methods need information on the number of invariable sites that were present in your dataset.
`-s` : input alignment file in fasta or phylip format
`-p` : random seed to start calculations for maximum likelihood
`-n` : name base for output files
`-T` : number of threads to use during analysis
Remark: some SNP datasets do not require a model of rate heterogeneity according to the standard model tests. When this is the case you can use, for instance, `-m ASC_GTRCAT` in combination with the `-V` option. This will make RAxML execute an inference under a plain GTR model without any correction for rate heterogeneity. Also note that, while it is possible to use the per-site rate model of rate heterogeneity (the CAT model) with an ascertainment bias correction, the RAxML developers recommend that you either use CAT in combination with `-V` (no rate heterogeneity) or the  corresponding Gamma model of rate heterogeneity with ascertainment bias correction (`-m ASC_GTRGAMMA` etc).
##### STEP 4: Make species trees from all gene trees using ASTRAL
For each gene tree, we can now make a species tree using [ASTRAL](https://github.com/smirarab/ASTRAL) (Zhang et al., 2018). For this we need a file (`all_locus_trees.txt`) with all gene trees as input. This file is made by concatenating all gene trees created in the previous step to a single file.
```
cat /folder/with/all/trees/RAxML_bestTree.* > /folder/with/all/trees/all_locus_trees.txt
```
You can run ASTRAL as follows with options –i the input file and –o the output file name (use the path to Astral on your computer):
```
java -Xmx5000M -jar /usr/local/bioinf/Astral/astral.5.6.2.jar -i all_locus_trees.txt -o astral_output.txt
```
The output file will contain a tree with posterior probability values on the branches. These local branch support values are more predictive of accuracy, and also tend to be higher than bootstraps (Sayyari and Mirarab, 2016).
### Run shell script to make trees from concatenated SNPs
#### How to run the script
The shell script can be run in a similar matter as the shell script to produce coalescent trees from the previous step.
Also here, you need to adjust the absolute path to the jModeltest jar to the path on your computer (line 50).

#### Detailed info on the different steps of the script
##### STEP 1: Convert genotypes file to fasta file
In this step the genotypes file is converted to multiple fasta files per locus. This is done using the custom perl script `convert_genotypes_to_concatenated_alignment.pl`.
```
perl convert_genotypes_to_concatenated_alignment.pl input_genotypes.txt output.fasta
```
The script takes as first argument the genotypes.txt file from step 2 and as second argument the name you choose as fasta output file. Note that if there are multiple alleles for a locus, the script will make a IUPAC consensus sequence and use that in the output.
##### STEP 2: Remove invariant sites from the alignments
Since you probably work with a subset of samples from the complete genotypes file, there are typically still invariant sites present in your subsetted genotypes file. In the phylogenetic method we want to apply, we need to correct for ascertainment bias (see step 4.2.3). To be able to do this, no invariant sites are allowed in your fasta alignment file. Therefore the invariant sites are removed using the script `ascbias.py` from  https://github.com/btmartin721/raxml_ascbias. The input file type for this script is `phylip` format. This means the fasta alignment file first needs to be converted to a phylip file. This can be done using the software `jModeltest` (use the path on your computer).
```
java -jar /usr/local/bioinf/jmodeltest2/dist/jModelTest.jar -d inputfile.fasta –getPhylip
```
After the conversion to phylip files, the invariant sites can be removed. We use the `ascbias.py` script from https://github.com/btmartin721/raxml_ascbias .
```
python3 ascbias.py -p input.fasta.phy -o outputfilenamebase
```
##### STEP 3: Make phylogenetic tree with bootstraps
Once the alignment file containing SNPs only is (in phylip or fasta format, in this case phylip format) available, you can construct phylogenetic trees using the software `RAxML`. `RAxML` uses maximum likelihood, and is computationally optimized to work with large datasets.
```
raxml -m ASC_GTRCAT –V --asc-corr=lewis -s inputalignment.phy -p 12345 -x 12345 -n name -T number_of_threads -w /absolute/path/to/output/folder -f a -# number_of_bootstraps >> raxml_log.txt
```
Options:
`-m` Evolutionary model to use. GTRGAMMA is the preferred model to use in RAxML. When calculations are too slow, you can use GTRCAT (recommended if you have more than 50 taxa). The “ASC_” is added because we want to correct for ascertainment bias. This is necessary when you work with variable sites only (in our case, concatenated SNPs).
`-V` : disable rate heterogeneity (widely applied if you work with multilocus data)
`--asc-corr` : the ascertainment bias correction model to be used. Here the simplest method is used (lewis correction). Other correction methods need information on the number of invariable sites that were present in your dataset.
`-s` : input alignment file in fasta or phylip format
`-p` : random seed to start calculations for maximum likelihood
`-#` : number of bootstraps
`-x` : random seed to start calculations for bootstrapping
`-n` : name base for output files
`-T` : number of threads to use during analysis
`-f a` : with the `–f` option, you can select the algorithm to use. `–f a` is the method to do both the calculation of the most likely tree and rapid bootstrapping in one run.
Remark: some SNP datasets do not require a model of rate heterogeneity according to the standard model tests. When this is the case you can use, for instance, `-m ASC_GTRCAT` in combination with the `-V` option. This will make RAxML execute an inference under a plain GTR model without any correction for rate heterogeneity. Also note that, while it is possible to use the per-site rate model of rate heterogeneity (the CAT model) with an ascertainment bias correction, the RAxML developers recommend that you either use CAT in combination with `-V` (no rate heterogeneity) or the  corresponding Gamma model of rate heterogeneity with ascertainment bias correction (`-m ASC_GTRGAMMA` etc).



## References

Boyer, F., Mercier, C., Bonin, A., Le Bras, Y., Taberlet, P., Coissac, E., 2016. OBITools: a Unix-inspired software package for DNA metabarcoding. Mol. Ecol. Resour. 16, 176-182.

Darriba, D., Taboada, G.L., Doallo, R., D., P., 2012. jModelTest 2: more models, new heuristics and parallel computing. Nat Methods 9, 772.

Elshire, R.J., Glaubitz, J.C., Sun, Q., Poland, J. a, Kawamoto, K., Buckler, E.S., Mitchell, S.E., 2011. A robust, simple genotyping-by-sequencing (GBS) approach for high diversity species. PLoS One 6, e19379.

Hapke, A., Thiele, D., 2016. GIbPSs: a toolkit for fast and accurate analyses of genotyping-by-sequencing data without a reference genome. Mol. Ecol. Resour. 16, 979-990.

Herten, K., Hestand, M.S., Vermeesch, J.R., Van Houdt, J.K., 2015. GBSX: a toolkit for experimental design and demultiplexing genotyping by sequencing experiments. BMC Bioinformatics 16, 1–6.

Martin, M., 2011. Cutadapt removes adapter sequences from high-throughput sequencing reads. EMBnet.journal 17, 10.

Poland, J. a, Brown, P.J., Sorrells, M.E., Jannink, J.-L., 2012. Development of high-density genetic maps for barley and wheat using a novel two-enzyme genotyping-by-sequencing approach. PLoS One 7, e32253.

Sayyari, E., Mirarab, S., 2016. Fast Coalescent-Based Computation of Local Branch Support from Quartet Frequencies. Mol. Biol. Evol. 33, 1654–1668.

Schmieder, R., Edwards, R., 2011. Quality control and preprocessing of metagenomic datasets. Bioinformatics 27, 863–864.

Stamatakis, A., 2014. RAxML version 8: A tool for phylogenetic analysis and post-analysis of large phylogenies. Bioinformatics 30, 1312–1313.

Zhang, C., Rabiee, M., Sayyari, E., Mirarab, S., 2018. ASTRAL-III: Polynomial time species tree reconstruction from partially resolved gene trees. BMC Bioinformatics 19, 15–30.

Zhang, J., Kobert, K., Flouri, T., Stamatakis, A., 2014. PEAR: A fast and accurate Illumina Paired-End reAd mergeR. Bioinformatics 30, 614–620.
