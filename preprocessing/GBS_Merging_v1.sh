#!/bin/bash

date

echo "-------"
echo "Merging"
echo "-------"

#####################################################################################################################################################################
# author: Annelies Haegeman, P21, Growth & Development
# version: 1
# 26/01/2016
#
# This script merges F and R reads using the software Pear
#
# This script makes use of the software Pear, which should be called as "pear"
#
# Example usage:
#			bash GBS_Merging_v1.sh \
#				--ListOfSamples="MyListOfSamples.lst" \
#				--ScriptsDirectory="ScriptsDirectory" \
#				--InputDirectory=../Demultiplexing \
#				--OutputDirectory=. \
#				--LogDirectory=. \
#				--MinOverlap=10 \
#				--MaxNumberOfCores=16 \
#				--MaxMemory="30G" \
#				--MinLength=25 \
#
#
#####################################################################################################################################################################

	# Accept arguments

		for ArgumentCounter in "$@" ;do

			case $ArgumentCounter in

				-l=*|--ListOfSamples=*)
				ListOfSamples="${ArgumentCounter#*=}"
				shift
				;;

				-sd=*|--ScriptsDirectory=*)
				ScriptsDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-id=*|--InputDirectory=*)
				InputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-od=*|--OutputDirectory=*)
				OutputDirectory="${ArgumentCounter#*=}"
				shift
				;;
				
				-ld=*|--LogDirectory=*)
				LogDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-mo=*|--MinOverlap=*)
				MinOverlap="${ArgumentCounter#*=}"
				shift
				;;

				-c=*|--MaxNumberOfCores=*)
				MaxNumberOfCores="${ArgumentCounter#*=}"
				shift
				;;

				-me=*|--MaxMemory=*)
				MaxMemory="${ArgumentCounter#*=}"
				shift
				;;

				-ml=*|--MinLength=*)
				MinLength="${ArgumentCounter#*=}"
				shift
				;;
				
				esac
			done

		echo "pwd = "$(pwd)

		echo "ListOfSamples = "$ListOfSamples
		echo "ScriptsDirectory = "$ScriptsDirectory
		echo "InputDirectory = "$InputDirectory
		echo "OutputDirectory = "$OutputDirectory
		echo "LogDirectory = "$LogDirectory		
		echo "MinOverlap = "$MinOverlap
		echo "MaxNumberOfCores = "$MaxNumberOfCores
		echo "MaxMemory = "$MaxMemory
		echo "MinLength = "$MinLength
		
		

		
# Initiate output table.

		ListOfSamples_output=$(echo $(echo $ListOfSamples | cut -f1 -d ".")"_Merging_output.tbl")
		echo -e "#Sample\tNumberOfMergedReads\tNumberForwardSingles\tNumberOfReverseSingles\tNumberOfDiscardedReads" > $ListOfSamples_output

# Start to loop over samples

		Samples=$(cat $ListOfSamples | grep -v "^#")

		NumberOfSamples=$(cat $ListOfSamples | grep -v "^#" | wc -l)
		echo -e "\nNumber of samples = "$NumberOfSamples

		Counter=1

		for Sample in $Samples; do
			pear -f $InputDirectory/"$Sample"_F_ResRemAndTrimmed.fq -r $InputDirectory/"$Sample"_R_ResRemAndTrimmed.fq -o $OutputDirectory/$Sample -p 1.0 -v $MinOverlap -j $MaxNumberOfCores -y $MaxMemory -n $MinLength
			#echo $Counter
			Counter=$(($Counter + 1))
			
			# Count reads.
			echo -e "\nCount reads"
					NumberOfMergedReads=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/"$Sample".assembled.fastq)
					NumberOfForwardSingles=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/"$Sample".unassembled.forward.fastq)
					NumberOfReverseSingles=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/"$Sample".unassembled.reverse.fastq)
					NumberOfDiscardedReads=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/"$Sample".discarded.fastq)
					
					TableOfBarcodes_output=$(echo $(echo $TableOfBarcodes | cut -f1 -d ".")"_output.tbl")

					echo -e $Sample"\t"$NumberOfMergedReads"\t"$NumberOfForwardSingles"\t"$NumberOfReverseSingles"\t"$NumberOfDiscardedReads >> $ListOfSamples_output

					echo "Sample = "$Sample
					echo "NumberOfMergedReads = "$NumberOfMergedReads
					echo "NumberOfForwardSingles = "$NumberOfForwardSingles
					echo "NumberOfReverseSingles = "$NumberOfReverseSingles
					echo "NumberOfDiscardedReads = "$NumberOfDiscardedReads
			
		done

	# Sort output on sample name, and print to screen.
		(head -n 1 $ListOfSamples_output && tail -n $NumberOfSamples $ListOfSamples_output | sort) > temp; cat temp > $ListOfSamples_output; rm temp
		echo ""
		cat $ListOfSamples_output
		
date