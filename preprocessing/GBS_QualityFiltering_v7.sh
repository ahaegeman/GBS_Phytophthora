#!/bin/bash

date

echo "-----------------"
echo "Quality Filtering"
echo "-----------------"

#####################################################################################################################################################################
# author: Christophe Verwimp, P39, Growth & Development, adjusted by Annelies Haegeman, P39
# Contact:	christophe.verwimp@ilvo.vlaanderen.be, annelies.haegeman@ilvo.vlaanderen.be
# version: 5
# 27/01/2016
#
# This script:	- Discards reads containing a maximum defined number of Ns.
#		- Filters reads on a minimum quality score for a minimum percentages of bases.
#		- Creates output table containing sample info: quality encoding, number of reads after discarding N's and number of reads after quality filtering.
#Version 5 of this script uses output of trimmed and merged reads from PE GBS. The merged reads are combined with unassembled forward and reverse reads in this step.
#It does NOT include automatic detection of Quality Encoding nor does it convert everything to Sanger format.
#
# This script makes use of:
#		- parallel							absolut path embeded in script, adjust this if needed
#		- FastQ_CountReads.py 						in ScriptsDirectory
#		- prinseq-lite-0.20.4						absolut path embedded in script, adjust this if needed
#
# Example usage:
#			bash GBS_QualityFiltering_v4.sh \
#				--ListOfSamples="MyListOfSamples.lst" \
#				--ScriptsDirectory="ScriptsDirectory" \
#				--InputDirectory=../Demultiplexing \
#				--OutputDirectory=. \
#				--LogDirectory=. \
#				--MinMeanQuality=25 \
#				--MinLength=40 \
#				--MaxNumberOfNs=0 \
#				--MaxNumberOfCores=16 \
#				--MinBaseQuality=20 \
#				--TrimmingLengthOfNonMergedReads=20
#
# Version changes (1 => 4):
#		- Use of GNU parallel instead of '& and wait' for parallelization.
# Version changes (4 => 6):
# 		changes made by Annelies Haegeman to fit PE GBS workflow
# Version changes (6 => 7):
# 		treat merged reads and remaining single reads separately
#####################################################################################################################################################################

# Function declaration

	function parallelize {

		Samples=$(cat $ListOfSamples | grep -v "^#")

		NumberOfSamples=$(cat $ListOfSamples | grep -v "^#" | wc -l)
		echo -e "\nNumber of samples = "$NumberOfSamples

		Counter=1

		for Sample in $Samples; do

			echo -e "$Counter"@"$MinMeanQuality\t$Sample\t$InputDirectory\t$OutputDirectory\t$ScriptsDirectory\t$LogDirectory\t$ListOfSamples_output\t$MinLength\t$MaxNumberOfNs\t$TrimmingLengthOfNonMergedReads\t$MinBaseQuality"

			Counter=$(($Counter + 1))
			
			done | perl /usr/local/bioinf/parallel --jobs $MaxNumberOfCores --colsep '\t' -- Filter {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}

		}

	function Filter {

		# Accept arguments

			Counter_MinMeanQuality=$1
			Sample=$2
			InputDirectory=$3
			OutputDirectory=$4
			ScriptsDirectory=$5
			LogDirectory=$6
			ListOfSamples_output=$7
			MinLength=$8
			MaxNumberOfNs=$9
			TrimmingLengthOfNonMergedReads=${10}	#!! do not use $10, then it will take $1 and add a zero to it.
			MinBaseQuality=${11}



			Counter=$(echo $Counter_MinMeanQuality | cut -f1 -d "@")
			MinMeanQuality=$(echo $Counter_MinMeanQuality | cut -f2 -d "@")

			echo -e "\n==> Counter = "$Counter


				#Mask low quality bases with N
					fastq_masker -q $MinBaseQuality -r N -i $InputDirectory/$Sample.assembled.fastq -o $OutputDirectory/$Sample"_merged_low_qual_replaced.fastq" -v
					fastq_masker -q $MinBaseQuality -r N -i $InputDirectory/$Sample".unassembled.forward.fastq" -o $OutputDirectory/$Sample"_forward_low_qual_replaced.fastq" -v
					fastq_masker -q $MinBaseQuality -r N -i $InputDirectory/$Sample".unassembled.reverse.fastq" -o $OutputDirectory/$Sample"_reverse_low_qual_replaced.fastq" -v

				# Filter assembled (or merged) reads on quality.
					echo -e "\nQuality filtering of merged reads."
					prinseq-lite -fastq $OutputDirectory/$Sample"_merged_low_qual_replaced.fastq" -out_good $OutputDirectory/$Sample"_merged_filtered" -out_bad $OutputDirectory/$Sample"_merged_discarded" -min_qual_mean $MinMeanQuality -min_len $MinLength -ns_max_p $MaxNumberOfNs
												
					#rename 's/.fastq//' $OutputDirectory/* -f
					mv $OutputDirectory/$Sample"_merged_filtered.fastq" $OutputDirectory/$Sample"_merged_filtered.fq"
					mv $OutputDirectory/$Sample"_merged_discarded.fastq" $OutputDirectory/$Sample"_merged_discarded.fq"
					
				# Filter non merged reads (as pairs) on quality.
					echo -e "\nQuality filtering of non-merged reads."
					prinseq-lite -fastq $OutputDirectory/$Sample"_forward_low_qual_replaced.fastq" -fastq2 $OutputDirectory/$Sample"_reverse_low_qual_replaced.fastq" -out_good $OutputDirectory/$Sample"_remaining_pairs_filtered" -out_bad $OutputDirectory/$Sample"_discarded" -min_qual_mean $MinMeanQuality -min_len $MinLength -trim_right $TrimmingLengthOfNonMergedReads -ns_max_p $MaxNumberOfNs
												
					#rename 's/.fastq//' $OutputDirectory/* -f
					mv $OutputDirectory/$Sample"_remaining_pairs_filtered_1.fastq" $OutputDirectory/$Sample"_remaining_pairs_filtered_F.fq"
					mv $OutputDirectory/$Sample"_remaining_pairs_filtered_2.fastq" $OutputDirectory/$Sample"_remaining_pairs_filtered_R.fq"
					mv $OutputDirectory/$Sample"_discarded_1.fastq" $OutputDirectory/$Sample"_F_discarded.fq"
					mv $OutputDirectory/$Sample"_discarded_2.fastq" $OutputDirectory/$Sample"_R_discarded.fq"
					mv $OutputDirectory/$Sample"_remaining_pairs_filtered_1_singletons.fastq" $OutputDirectory/$Sample"_F_singletons_filtered.fq"
					mv $OutputDirectory/$Sample"_remaining_pairs_filtered_2_singletons.fastq" $OutputDirectory/$Sample"_R_singletons_filtered.fq"

				#remove file with masked Ns and discarded sequences to save disk space
				  rm $OutputDirectory/$Sample"_merged_low_qual_replaced.fastq"
				  rm $OutputDirectory/$Sample"_forward_low_qual_replaced.fastq"
				  rm $OutputDirectory/$Sample"_reverse_low_qual_replaced.fastq"
				  rm $OutputDirectory/$Sample"_F_discarded.fq"
				  rm $OutputDirectory/$Sample"_R_discarded.fq"
				  rm $OutputDirectory/$Sample"_merged_discarded.fq"
				  
				
				# Count number of reads, count number of reads with internal RE sites
					NumberOfMergedReadsAfterQualityFiltering=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$Sample"_merged_filtered.fq")
					NumberOfRemainingPairsAfterQualityFiltering=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$Sample"_remaining_pairs_filtered_F.fq")
					NumberOfForwardSingletonsAfterQualityFiltering=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$Sample"_F_singletons_filtered.fq")
					NumberOfReverseSingletonsAfterQualityFiltering=$(python $ScriptsDirectory/FastQ_CountReads.py -i $OutputDirectory/$Sample"_R_singletons_filtered.fq")
					NumberOfMergedReadsWithMspISite=`obigrep --without-progress-bar -s 'CCGG' $OutputDirectory/$Sample"_merged_filtered.fq" | obicount -a --without-progress-bar`
					NumberOfMergedReadsWithPstISite=`obigrep --without-progress-bar -s 'CTGCAG' $OutputDirectory/$Sample"_merged_filtered.fq" | obicount --without-progress-bar -a`
					NumberOfForwardRemainingPairsWithMspISite=`obigrep --without-progress-bar -s 'CCGG' $OutputDirectory/$Sample"_remaining_pairs_filtered_F.fq" | obicount --without-progress-bar -a`
					NumberOfForwardRemainingPairsWithPstISite=`obigrep --without-progress-bar -s 'CTGCAG' $OutputDirectory/$Sample"_remaining_pairs_filtered_F.fq" | obicount --without-progress-bar -a`
					NumberOfReverseRemainingPairsWithMspISite=`obigrep --without-progress-bar -s 'CCGG' $OutputDirectory/$Sample"_remaining_pairs_filtered_R.fq" | obicount --without-progress-bar -a`
					NumberOfReverseRemainingPairsWithPstISite=`obigrep --without-progress-bar -s 'CTGCAG' $OutputDirectory/$Sample"_remaining_pairs_filtered_R.fq" | obicount --without-progress-bar -a`					
					NumberOfForwardSingletonsWithMspISite=`obigrep --without-progress-bar -s 'CCGG' $OutputDirectory/$Sample"_F_singletons_filtered.fq" | obicount --without-progress-bar -a`
					NumberOfForwardSingletonsWithPstISite=`obigrep --without-progress-bar -s 'CTGCAG' $OutputDirectory/$Sample"_F_singletons_filtered.fq" | obicount --without-progress-bar -a`
					NumberOfReverseSingletonsWithMspISite=`obigrep --without-progress-bar -s 'CCGG' $OutputDirectory/$Sample"_R_singletons_filtered.fq" | obicount --without-progress-bar -a`
					NumberOfReverseSingletonsWithPstISite=`obigrep --without-progress-bar -s 'CTGCAG' $OutputDirectory/$Sample"_R_singletons_filtered.fq" | obicount --without-progress-bar -a`
					
				#merge singleton files into 1 file and discard F and R singletons files
				cat $OutputDirectory/$Sample"_F_singletons_filtered.fq" $OutputDirectory/$Sample"_R_singletons_filtered.fq" > $OutputDirectory/$Sample"_singletons_filtered.fq"
				rm $OutputDirectory/$Sample"_F_singletons_filtered.fq"
				rm $OutputDirectory/$Sample"_R_singletons_filtered.fq"	
					
				#write reads without internal restriction sites to new files for merged files
				obigrep --without-progress-bar -s 'CCGG' -v $OutputDirectory/$Sample"_merged_filtered.fq" > $OutputDirectory/$Sample"_merged_filtered_woMspI.fq"	#first select sequences that do not have MspI Site
				obigrep --without-progress-bar -s 'CTGCAG' -v $OutputDirectory/$Sample"_merged_filtered_woMspI.fq" > $OutputDirectory/$Sample"_merged_filtered_woREsites.fq"	#now select sequences that do not have PstI Site from file of sequences that do not have MspI Site
				rm $OutputDirectory/$Sample"_merged_filtered_woMspI.fq" #remove file with sequences without MspI sites
				
				#write reads without internal restriction sites to new files for singletons
				obigrep --without-progress-bar -s 'CCGG' -v $OutputDirectory/$Sample"_singletons_filtered.fq" > $OutputDirectory/$Sample"_singletons_filtered_woMspI.fq"	#first select sequences that do not have MspI Site
				obigrep --without-progress-bar -s 'CTGCAG' -v $OutputDirectory/$Sample"_singletons_filtered_woMspI.fq" > $OutputDirectory/$Sample"_singletons_filtered_woREsites_temp.fq"	#now select sequences that do not have PstI Site from file of sequences that do not have MspI Site
				
				#write reads without internal restriction sites to new files for paired reads
				rm $OutputDirectory/$Sample"_singletons_filtered_woMspI.fq" #remove file with sequences without MspI sites				
				obigrep --without-progress-bar -s 'CCGG' -v $OutputDirectory/$Sample"_remaining_pairs_filtered_F.fq" > $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woMspI.fq"	#first select sequences that do not have MspI Site
				obigrep --without-progress-bar -s 'CTGCAG' -v $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woMspI.fq" > $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_temp.fq"	#now select sequences that do not have PstI Site from file of sequences that do not have MspI Site
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woMspI.fq" #remove file with sequences without MspI sites
				obigrep --without-progress-bar -s 'CCGG' -v $OutputDirectory/$Sample"_remaining_pairs_filtered_R.fq" > $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woMspI.fq"	#first select sequences that do not have MspI Site
				obigrep --without-progress-bar -s 'CTGCAG' -v $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woMspI.fq" > $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_temp.fq"	#now select sequences that do not have PstI Site from file of sequences that do not have MspI Site
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woMspI.fq" #remove file with sequences without MspI sites
				
				#repair broken pairs for files without restriction sites
				pairfq makepairs -f $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_temp.fq" -r $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_temp.fq" -fp $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites.fq" -rp $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites.fq" -fs $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_singletons.fq" -rs $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_singletons.fq"
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_temp.fq"
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_temp.fq"
				#concatenate all singleton files and remove intermediate files
				cat $OutputDirectory/$Sample"_singletons_filtered_woREsites_temp.fq" $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_singletons.fq" $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_singletons.fq" > $OutputDirectory/$Sample"_singletons_filtered_woREsites.fq"
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_F_woREsites_singletons.fq"
				rm $OutputDirectory/$Sample"_remaining_pairs_filtered_R_woREsites_singletons.fq"
				rm $OutputDirectory/$Sample"_singletons_filtered_woREsites_temp.fq"
				
				# Output info.

				echo -e "$Sample\t$NumberOfMergedReadsAfterQualityFiltering\t$NumberOfRemainingPairsAfterQualityFiltering\t$NumberOfForwardSingletonsAfterQualityFiltering\t$NumberOfReverseSingletonsAfterQualityFiltering\t$NumberOfMergedReadsWithPstISite\t$NumberOfMergedReadsWithMspISite\t$NumberOfForwardRemainingPairsWithPstISite\t$NumberOfForwardRemainingPairsWithMspISite\t$NumberOfReverseRemainingPairsWithPstISite\t$NumberOfReverseRemainingPairsWithMspISite\t$NumberOfForwardSingletonsWithPstISite\t$NumberOfForwardSingletonsWithMspISite\t$NumberOfReverseSingletonsWithPstISite\t$NumberOfReverseSingletonsWithMspISite" >> $ListOfSamples_output
					
		}

		export -f Filter

# Start

	# Accept arguments

		for ArgumentCounter in "$@" ;do

			case $ArgumentCounter in

				-l=*|--ListOfSamples=*)
				ListOfSamples="${ArgumentCounter#*=}"
				shift
				;;

				-sd=*|--ScriptsDirectory=*)
				ScriptsDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-id=*|--InputDirectory=*)
				InputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-od=*|--OutputDirectory=*)
				OutputDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-ld=*|--LogDirectory=*)
				LogDirectory="${ArgumentCounter#*=}"
				shift
				;;

				-mq=*|--MinMeanQuality=*)
				MinMeanQuality="${ArgumentCounter#*=}"
				shift
				;;

				-c=*|--MaxNumberOfCores=*)
				MaxNumberOfCores="${ArgumentCounter#*=}"
				shift
				;;

				-ml=*|--MinLength=*)
				MinLength="${ArgumentCounter#*=}"
				shift
				;;

				-mn=*|--MaxNumberOfNs=*)
				MaxNumberOfNs="${ArgumentCounter#*=}"
				shift
				;;

				-tr=*|--TrimmingLengthOfNonMergedReads=*)
				TrimmingLengthOfNonMergedReads="${ArgumentCounter#*=}"
				shift
				;;
				
				-mb=*|--MinBaseQuality=*)
				MinBaseQuality="${ArgumentCounter#*=}"
				shift
				;;
				
				esac
			done

		echo "pwd = "$(pwd)

		echo "ListOfSamples = "$ListOfSamples
		echo "ScriptsDirectory = "$ScriptsDirectory
		echo "InputDirectory = "$InputDirectory
		echo "OutputDirectory = "$OutputDirectory
		echo "LogDirectory = "$LogDirectory
		echo "MinLength = "$MinLength
		echo "MinMeanQuality = "$MinMeanQuality
		echo "MaxNumberOfNs = "$MaxNumberOfNs
		echo "TrimmingLengthOfNonMergedReads = "$TrimmingLengthOfNonMergedReads
		echo "MaxNumberOfCores = "$MaxNumberOfCores
		echo "MinBaseQuality = "$MinBaseQuality
		

	# Initiate output table.

		ListOfSamples_output=$(echo $(echo $ListOfSamples | cut -f1 -d ".")"_Quality_Filtering_output.tbl")
		echo -e "Sample\tNumberOfMergedReadsAfterQualityFiltering\tNumberOfRemainingPairsAfterQualityFiltering\tNumberOfForwardSingletonsAfterQualityFiltering\tNumberOfReverseSingletonsAfterQualityFiltering\tNumberOfMergedReadsWithPstISite\tNumberOfMergedReadsWithMspISite\tNumberOfForwardRemainingPairsWithPstISite\tNumberOfForwardRemainingPairsWithMspISite\tNumberOfReverseRemainingPairsWithPstISite\tNumberOfReverseRemainingPairsWithMspISite\tNumberOfForwardSingletonsWithPstISite\tNumberOfForwardSingletonsWithMspISite\tNumberOfReverseSingletonsWithPstISite\tNumberOfReverseSingletonsWithMspISite" > $ListOfSamples_output

	# Call parallelize.

		parallelize

	# Sort output on sample name, and print to screen.

		(head -n 1 $ListOfSamples_output && tail -n $NumberOfSamples $ListOfSamples_output | sort) > temp; cat temp > $ListOfSamples_output; rm temp
		echo ""
		cat $ListOfSamples_output
		
date