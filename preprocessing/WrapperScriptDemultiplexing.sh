#!/bin/bash

####################################################################################################################################################################################################
# Author:	Christophe Verwimp, P21, Growth & Development
# Contact:	christophe.verwimp@ilvo.vlaanderen.be
# Date:		14/01/2016
#
# This script demultiplexes one FastQ file on inline barcodes and removes the barcodes from the reads. A table containing the number of reads per sample is created.
#
# Procedure:	
# 				1. Create a tab separated file, without a header, containing following information:
#
#						column 1	sample name
#						column 2	barcode sequence
#						column 3	restriction enzyme (compatible to barcode adapter)
#						column 4	optionally, second restriction enzyme
#						column 5	optionally, second barcode sequence
#
#						Save this file as (f.e) Barcodes.tbl in the Demultiplexing directory.
#
#						This version processes only one raw FastQ at a time, be sure to use unique sample names if you want to repeat it on a second raw FastQ file.
#
#				2. Adjust arguments below, if needed (avoid spaces!):
#
# 						- Add the names (including absolute path) of your raw FastQ files in the RawData arguments. If SE, RawDataReverse="".
#						- Add the name (including absolute path) to the TableOfBarcodes argument.
#						- Adjust the number of mismatches allowed for recognizing barcodes.
#
#				3. Open a terminal, navigate to the Demultiplexing directory and execute following command:
#
#						bash WrapperScriptDemultiplexing.sh
#
#				4. A new file is created (f.e) RawData_NumberOfReads.tbl, containing the sample names and the numbers of reads per sample after demultiplexing.
#
# This script requires:
#
#			- Python 2.7.6
#			- GBSX v1.0
#			- GBS_Demultiplexing_v2.sh
#			- FastQ_CountReads.py
#
# Notes:
#			- Raw FastQ file should be unzipped.
#			- The input is one raw FastQ.
#			- An F for 'forward' and R for 'reverse' is added to the names of sample specific FastQ files.
#			- The Barcodes.tbl file is ordered on descending barcode length by GBS_Demultiplexing_v1.sh to avoid wrong assigned when using high numbers of allowed mismatches.
#			- Undetermined reads are stored in a seperate FastQ file.
#
####################################################################################################################################################################################################

# Arguments

	ScriptsDirectory="/home/genomics/kvanpoucke/Run12/Scripts/Scripts"
	OutputDirectory="/home/genomics/kvanpoucke/Run12/Preprocessing/1_Demultiplexing/Output"
	LogDirectory="/home/genomics/kvanpoucke/Run12/Preprocessing/1_Demultiplexing/Log"
	RawDataForward="/home/genomics/kvanpoucke/Run12/Raw_Data/GP3288074_run12_S1_R1_001.fastq"
	RawDataReverse="/home/genomics/kvanpoucke/Run12/Raw_Data/GP3288074_run12_S1_R2_001.fastq"
	TableOfBarcodes="/home/genomics/kvanpoucke/Run12/Preprocessing/TableOfBarcodes.txt"
	NumberOfMismatchesAllowed=1

# Function declaration

	function demultiplex {

		bash $ScriptsDirectory/GBS_Demultiplexing_v2.sh \
				--RawDataForward=$RawDataForward \
				--RawDataReverse=$RawDataReverse \
				--TableOfBarcodes=$TableOfBarcodes \
				--NumberOfMismatchesAllowed=$NumberOfMismatchesAllowed \
				--ScriptsDirectory=$ScriptsDirectory \
				--OutputDirectory=$OutputDirectory \
				--LogDirectory=$LogDirectory \
			
		}

# Call function and write output to log file.
		
	demultiplex 2>&1 | tee $LogDirectory/$(echo "Demultiplexing_"$(date | sed 's/ /_/g')".log")
