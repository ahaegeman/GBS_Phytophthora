#!/bin/bash

####################################################################################################################################################################################################
# Author:	Annelies Haegeman, P21, Growth & Development
# Contact:	annelies.haegeman@ilvo.vlaanderen.be
# Date:		26/01/2016
#
# This script merges F and R reads with the software Pear.
#
# Procedure:	 Open a terminal, navigate to the Demultiplexing directory and execute following command:
##						bash WrapperScriptDemultiplexing.sh
##				A new file is created containing the sample names and the numbers of reads per sample after demultiplexing.
#
# This script requires:
#


#
####################################################################################################################################################################################################

# Arguments

	ScriptsDirectory="/home/genomics/kvanpoucke/Run12/Scripts/Scripts"
	OutputDirectory="/home/genomics/kvanpoucke/Run12/Preprocessing/3_Merging/Output"
	LogDirectory="/home/genomics/kvanpoucke/Run12/Preprocessing/3_Merging/Log"
	InputDirectory="/home/genomics/kvanpoucke/Run12/Preprocessing/2_Trimming/Output"
	ListOfSamples="/home/genomics/kvanpoucke/Run12/Preprocessing/ListOfSamples.txt"
	MinOverlap=10
	MaxNumberOfCores=24
	MaxMemory="30G"
	MinLength=25

# Function declaration

	function Merging {

		bash $ScriptsDirectory/GBS_Merging_v1.sh \
			--ListOfSamples=$ListOfSamples \
			--ScriptsDirectory=$ScriptsDirectory \
			--InputDirectory=$InputDirectory \
			--OutputDirectory=$OutputDirectory \
			--LogDirectory=$LogDirectory \
			--MinOverlap=$MinOverlap \
			--MaxNumberOfCores=$MaxNumberOfCores \
			--MaxMemory=$MaxMemory \
			--MinLength=$MinLength \
			
		}

# Call function and write output to log file.
		
	Merging 2>&1 | tee $LogDirectory/$(echo "Merging_"$(date | sed 's/ /_/g')".log")
