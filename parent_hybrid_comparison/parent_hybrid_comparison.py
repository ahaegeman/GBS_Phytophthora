#!/usr/bin/python3

#===============================================================================
# Description
#===============================================================================

# Annelies Haegeman, Oct 2017

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime
import multiprocessing
import pandas
import shelve

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Make identity similarity matrices from Gibpss output file "genotypes.txt"')

# Add mandatory arguments
parser.add_argument('genotypes',
                    help = 'Genotypes file ("genotypes.txt" as created by Gibpss software.')

# Add optional arguments
parser.add_argument('-s', '--snpsim',
                    default = 'snpsim.txt',
                    type = str,
                    help = 'Name of the output file which contains the identity/similarity values (default = snpsim.txt).')

parser.add_argument('-l', '--loci',
                    default = 'commonloci.txt',
                    type = str,
                    help = 'Name of the output file which contains the number of common loci (default = commonloci.txt).')
					
# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    """
    Print the current date and time to stderr.
    """
    sys.stderr.write('----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return

def compare_two_loci (locus1,locus2):
    """
    Compare two loci, output is a list of two values, 'similar' and 'missing'.
    Similar is 1 if at least 1 allele is shared between loci. Similar is 0 when one (or both) loci are missing data, or when no alleles are shared.
    Missing is 1 if one of the two (or both) loci were missing data.
    """    
    #initialize variables
    missing=0
    i=0
    check='no'
    similar=0   #define variable similar, this will be set to 1 if the two loci share at least 1 allele, and this is the output of the function
    if ((locus1 == '-999' and locus2 != '-999') or (locus2 == '-999' and locus1 != '-999')):
        missing=1
    #both individuals have this locus as missing    
    elif (locus1 == '-999' and locus2 == '-999'):
        missing=1        
    elif (locus1.find('/') == -1 and locus2.find('/') == -1):    #both individuals only have 1 allele (=both are homozygous) (both should NOT have slashes in their locus value)   
        if (locus1 == locus2):
            similar=1
    #indv1 has only 1 allele while individual 2 has more than one allele
    elif (locus1.find('/') == -1 and locus2.find('/') != -1):
        #split hybrid locus (locus of indv 2) into alleles
        alleles=locus2.split('/')
        #loop over alleles and check if one of the alleles matches the allele of indv1
        for allele in alleles:
            #print ("test of ",allele," against ",lociindv1[x])
            if allele == locus1:
                similar=1
    #indv1 has more than 1 allele (heterozygous)
    elif (locus1.find('/') != -1):
        #split parent (indv1) and hybrid (indv2) locus into alleles
        allelesindv1=locus1.split('/')            
        allelesindv2=locus2.split('/')
        #compare alleles in hybrid to alleles in parent, once a match is found, set variables "check" to "yes"
        for allele in allelesindv2: 
            while (i < len(allelesindv1)):
                if (allele == allelesindv1[i]):
                    check='yes'
                i=i+1
            i=0
        #check if a match was found when comparing the alleles    
        if check=='yes':
            similar=1
        check='no' 
    return(similar,missing)

def compare_two_loci_lists (lociindv1,lociindv2):
    """
    Compare the alleles of two individuals (= two tuples) containing data of all loci
    """
    all_loci_missing=0
    all_loci_similar=0
    #loop over all loci and compare each locus between two individuals
    for x in range(0,len(lociindv1)):
        #print ("locus ",x, end='\t')
        #print(lociindv1[x], end='\t')
        #print(lociindv2[x])
        #compare the two loci,
        similar,missing=compare_two_loci(lociindv1[x],lociindv2[x])
        all_loci_missing=all_loci_missing+missing
        all_loci_similar=all_loci_similar+similar        

    #calculate number of compared = common loci
    number_of_loci=len(lociindv1)
    compared_loci=number_of_loci-all_loci_missing
    #calculate similarity percentages
    if (compared_loci != 0):
        pct_hybrid_has_locus_that_corresponds_to_parent=all_loci_similar/compared_loci*100
    if (compared_loci == 0):   
        pct_hybrid_has_locus_that_corresponds_to_parent=0

    return{'pct_hybrid_has_locus_that_corresponds_to_parent':pct_hybrid_has_locus_that_corresponds_to_parent,
           'loci_in_common':compared_loci}

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    
    print_date()

"""   
PART 1: READ AND STORE DATA
"""
#load data as file genotypes.txt from Gibpps pipeline
genotypes = pandas.read_table(args['genotypes'], sep='\t',index_col=0,dtype=str)
#genotypes = pandas.read_table(args['genotypes'], sep='\t',index_col=0,low_memory=False,dtype=str)
#drop the last column if the output of Gibpps contains an empty column at the end of the file
#genotypes.drop(genotypes.columns[len(genotypes.columns)-1], axis=1, inplace=True)
#replace "-" to "_" in row names
#genotypes.columns = genotypes.columns.str.replace("-","_")
#transpose genotypes dataframe
transposed = genotypes.transpose()
#make a tuple containing all individuals, you can access individuals by doing tuple_indvs[0]
tuple_indvs = transposed.index.values.tolist()
#print (tuple_indvs)

# initiate a new shelve file to store all data
shelf = shelve.open('genotypes.shelve', 'c')
#loop over all individuals, make a shelve with as key the individual name, and as value a tuple containing the values for the loci (= certain column with index "iloc" x)
for x in range(0,len(transposed.index)):
   shelf[tuple_indvs[x]] = transposed.iloc[x].values.astype(str).tolist()   #make sure to save the values as strings (otherwise you can not look for "/" characters later)
   shelf[tuple_indvs[x]] = transposed.iloc[x].values.tolist()
   #df.values.astype(int).tolist()
shelf.close()     

"""
PART 2: CALCULATE SIMILARITIES BETWEEN SELECTED INDIVIDUALS AND WRITE TO OUTPUT
"""        
#make similarity matrix by defining a number of tasks per individual = which individuals should be compared to each other
#for each comparison that has to be made, look up the corresponding loci in the shelf and execute the compare loci function
#write the correct results for each task to the output

#initialize output files
# print header to output file snpsim
out_snpsim = open(args['snpsim'], 'w')    #open filehandle for idsim output file
print('\t',end="",file = out_snpsim) #start line with a tab
print('\t'.join(map(str,tuple_indvs)),file = out_snpsim) #print all individuals separated by tab
# print header to output file loci
out_loci = open(args['loci'], 'w')    #open filehandle for idsim output file
print('\t',end="",file = out_loci) #start line with a tab
print('\t'.join(map(str,tuple_indvs)),file = out_loci) #print all individuals separated by tab

#set counting variable
j=0
#initialize a list for the tasks
tasks=[]
#open the shelf for the entire process of looking up values
with shelve.open('genotypes.shelve', 'r') as shelf:
#loop over all individuals
    for j in range(0,len(tuple_indvs)):
    #for j in range(0,3):
        #print individual to beginning of line in output files    
        print(tuple_indvs[j],end='\t',file=out_snpsim)
        print(tuple_indvs[j],end='\t',file=out_loci)
        #set counter up to the individual number to define the correct tasks (the later the individual in the lists, the more comparison tasks need to be done)
        for i in range(0,j+1):
            #print("task ",i," =indv ",tuple_indvs[i],"vs task ",j," =indv ",tuple_indvs[j])
            #add a tuple to the existing list containing the two individuals that need to compared to each other
            #tasks=tasks+((tuple_indvs[i],tuple_indvs[j]),)
            tasks.append((tuple_indvs[i],tuple_indvs[j]))
        #once the tasks are defined for each individual, loop over the list tasks, do the comparison and print relevant results to output files
        for task in tasks:  #each task consists of a list of two individuals, task[0] is the name of the first individual, task[1] is the name of the second individual
            a=shelf[task[0]]
            b=shelf[task[1]]
            comparison=compare_two_loci_lists (a,b)
            print(comparison['loci_in_common'],end='\t',file=out_loci)
            print(format(comparison['pct_hybrid_has_locus_that_corresponds_to_parent'], '.2f'),end='\t',file=out_snpsim)
        #empty the tasks before going to the next individual
        tasks=[]  
        print('',file=out_snpsim)   #write a newline character to the output after finishing the tasks for one individual
        print('',file=out_loci)   #write a newline character to the output after finishing the tasks for one individual   
#close shelve and output files
shelf.close()
out_snpsim.close()
out_loci.close()

print_date()